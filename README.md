ansible-role-k8s-rolling-update
===============================

Update Operating System and reboot Kubernetes control plane and worker nodes. 
Written to work with Kubernetes cluster and Ansible AWX. 

Requirements
------------

None

Role Variables
--------------

See defaults/main.yml

Dependencies
------------

None

Example Playbook
----------------

See examples/playbook.yml and examples/hosts.example 

License
-------

BSD

Author Information
------------------

Javier Matos (https://bitbucket.org/fjmatos/ansible-role-k8s-rolling-update)
Based on https://github.com/kevincoakley/ansible-role-k8s-rolling-update
